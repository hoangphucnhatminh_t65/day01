<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Đăng Nhập</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<form class="form">
    <div class="center-form">
        <div class="table">
            <label class="label"> Họ và Tên </label>
            <label>
                <input type="text" class="input" required>
            </label>
        </div>
        <div class="table">
            <label class="label"> Giới tính </label>
            <div class="radio">
            <?php
            $gioi_tinh = array(0 => "Nam", 1 => "Nữ");
            foreach ($gioi_tinh as $key => $value) {
                echo '<input type="radio" id="gioi_tinh_' . $key . '" name="gioi_tinh" value="' . $key . '" ' . ($key == 0 ? 'checked' : '') . '>';
                echo '<label for="gioi_tinh_' . $key . '">' . $value . '</label>';
            }
            ?>
            </div>
        </div>
        <div class="table">
            <label class="label"> Phân khoa </label>
            <label>
                <select name="state" class="Department" >
                    <option disabled selected hidden> --Chọn phân khoa--</option>
                    <?php
                    $khoa = array("MAT" => "Khoa học máy tính", 
                                  "KDL" => "Khoa học vật liệu");
                    foreach ($khoa as $key => $value) {
                        echo '<option value= "' . $key . '">' . $value . '</option>';
                    }
                    ?>
                </select>
            </label>
        </div>
        <div class="center-btn">
            <input type="submit" class="submit-btn" value="Đăng Ký">
        </div>
    </div>
</form>
</body>
</html>