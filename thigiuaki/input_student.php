<!DOCTYPE html>
<html>
<head>
    <title>Form Đăng Ký Sinh Viên</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <style>
        .error-message {
            color: red;
        }
    </style>
    <script>
        function loadCities() {
            var thanhPho = document.getElementById("thanhPho");
            thanhPho.innerHTML = "";
            var option1 = document.createElement("option");
            option1.value = "Chonthanhpho";
            option1.text = "Chọn Thành Phố";
            var option2 = document.createElement("option");
            option2.value = "HaNoi";
            option2.text = "Tp Hà Nội";
            
            var option3 = document.createElement("option");
            option3.value = "HoChiMinh";
            option3.text = "Tp Hồ Chí Minh";
            
            thanhPho.add(option1);
            thanhPho.add(option2);
            thanhPho.add(option3);
        }

        function loadDistricts() {
            var quan = document.getElementById("quan");
            quan.innerHTML = "";

            var thanhPho = document.getElementById("thanhPho");
            var selectedCity = thanhPho.options[thanhPho.selectedIndex].value;

            if (selectedCity === "Chonthanhpho") {
                var option1 = document.createElement("option");
                option1.value = "Chonquan";
                option1.text = "Chọn Quận";
                quan.add(option1);
            } else if (selectedCity === "HaNoi") {
                var option1 = document.createElement("option");
                option1.value = "HoangMai";
                option1.text = "Hoàng Mai";
                var option2 = document.createElement("option");
                option2.value = "ThanhTri";
                option2.text = "Thanh Trì";
                var option3 = document.createElement("option");
                option3.value = "NamTuLiem";
                option3.text = "Nam Từ Liêm";
                var option4 = document.createElement("option");
                option4.value = "HaDong";
                option4.text = "Hà Đông";
                var option5 = document.createElement("option");
                option5.value = "CauGiay";
                option5.text = "Cầu Giấy";

                quan.add(option1);
                quan.add(option2);
                quan.add(option3);
                quan.add(option4);
                quan.add(option5);
            } else if (selectedCity === "HoChiMinh") {
                var option1 = document.createElement("option");
                option1.value = "Quan1";
                option1.text = "Quận 1";
                var option2 = document.createElement("option");
                option2.value = "Quan2";
                option2.text = "Quận 2";
                var option3 = document.createElement("option");
                option3.value = "Quan3";
                option3.text = "Quận 3";
                var option4 = document.createElement("option");
                option4.value = "Quan7";
                option4.text = "Quận 7";
                var option5 = document.createElement("option");
                option5.value = "Quan9";
                option5.text = "Quận 9";

                quan.add(option1);
                quan.add(option2);
                quan.add(option3);
                quan.add(option4);
                quan.add(option5);
            }
        }

        function validateForm() {
            var hoTen = document.getElementById("hoTen").value;
            var gioiTinh = document.querySelector('input[name="gioiTinh"]:checked');
            var thanhPho = document.getElementById("thanhPho").value;
            var quan = document.getElementById("quan").value;

            var error = document.getElementById("error");
            error.innerHTML = "";

            var hasError = false;

            if (!hoTen) {
                error.innerHTML += "Xin vui lòng điền Họ và tên.<br>";
                hasError = true;
            }

            if (!gioiTinh) {
                error.innerHTML += "Vui lòng chọn Giới tính.<br>";
                hasError = true;
            }

            if (thanhPho === "Chonthanhpho") {
                error.innerHTML += "Vui lòng chọn Thành phố.<br>";
                hasError = true;
            }

            if (quan === "Chonquan" || quan === "") {
                error.innerHTML += "Vui lòng chọn Quận.<br>";
                hasError = true;
            }

            if (hasError) {
                error.style.display = "block";
                return false;
            } else {
                error.style.display = "none";
                return true;
            }
        }
    </script>
</head>
<body>
    <h2>Form Đăng Ký Sinh Viên</h2>
    <p id="error" class="error-message" style="display: none;"></p>
    <form action="process_student.php" method="post" onsubmit="return validateForm()">
        <label for="hoTen">1. Họ và tên:</label>
        <input type="text" id="hoTen" name="hoTen" required><br><br>

        <label>2. Giới tính:</label>
        <input type="radio" id="nam" name="gioiTinh" value="Nam">
        <label for="nam">Nam</label>
        <input type="radio" id="nu" name="gioiTinh" value="Nữ">
        <label for="nu">Nữ</label><br><br>

        <label for="ngaySinh">3. Ngày sinh:</label>
        <select id="namSinh" name="namSinh">
            <?php
            for ($i = date("Y"); $i >= 1900; $i--) {
                echo "<option value='$i'>$i</option>";
            }
            ?>
        </select>
        <select id="thangSinh" name="thangSinh">
            <?php
            for ($i = 1; $i <= 12; $i++) {
                echo "<option value='$i'>$i</option>";
            }
            ?>
        </select>
        <select id="ngaySinh" name="ngaySinh">
            <?php
            for ($i = 1; $i <= 31; $i++) {
                echo "<option value='$i'>$i</option>";
            }
            ?>
        </select><br><br>

        <label for="thanhPho">4. Địa chỉ (Thành phố):</label>
        <select id="thanhPho" name="thanhPho" onchange="loadDistricts()">
            <option value="Chonthanhpho">Chọn thành phố</option>
        </select>

        <label for="quan">Quận:</label>
        <select id="quan" name="quan">
            <option value="Chonquan">Chọn quận</option>
        </select><br><br>

        <label for="thongTinKhac">5. Thông tin khác:</label>
        <textarea id="thongTinKhac" name="thongTinKhac" rows="4" cols="50"></textarea><br><br>

        <input type="submit" value="Đăng ký">
    </form>
    <script>
        loadCities();
    </script>
</body>
</html>
