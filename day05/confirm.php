<!DOCTYPE html>
<html lang="en">
<head>
    <title>Xác nhận thông tin đăng ký</title>
    <link rel="stylesheet" href="index.css" type="text/css">
</head>
<body>

<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $target_dir = "upload/";
    $target_file = $target_dir ."upload_image.jpg";
    move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);
}
?>
<form class="submit-form" id=" submit_form">
    <div class="center-form">
        <div class="check">
            <label class="label"> Họ và Tên <span class="validate">*</span> </label>
            <label class="font">
                <?php
                echo "" . $_POST["ho_ten"] . "<br>";
                ?>
            </label>
        </div>
        <div class="check">
            <label class="label"> Giới tính <span class="validate">*</span> </label>
            <label class="font">
                <?php
                echo "" . $_POST["gioi_tinh"] . "<br>";
                ?>
            </label>
        </div>
        <div class="check">
            <label class="label"> Phân Khoa <span class="validate">*</span> </label>
            <label class="font">
                <?php
                echo "" . $_POST["Department"] . "<br>";
                ?>
            </label>
        </div>
        <div class="check">
            <label class="label"> Ngày sinh <span class="validate">*</span> </label>
            <label class="font">
                <?php
                $timestamp = strtotime(str_replace('/', '-', $_POST["ngay_sinh"]));
                $validate_ngaysinh = date('d/m/Y', $timestamp);
                echo "" . $validate_ngaysinh . "<br>";
                ?>
            </label>
        </div>
        <div class="check">
            <label class="label"> Địa chỉ <span class="validate">*</span> </label>
            <label class="font">
                <?php
                echo "" . $_POST["address"] . "<br>";
                ?>
            </label>
        </div>
        <div class="check">
            <label class="label"> Hình ảnh <span class="validate">*</span> </label>
            <label class="font">
                <img src="upload/upload_image.jpg" width="50" height="50" alt="loi hình ảnh">
            </label>
        </div>
        <div class="center-btn">
            <input type="submit" id="submit" class="submit-btn" value="Xác nhận">
        </div>
    </div>
</form>
</body>
</html>
