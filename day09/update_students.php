<!DOCTYPE html>
<html lang="en">
<head>
    <title>Update Student Information</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <!-- Include necessary stylesheets and scripts -->
    <style>
        body {
            background-color: #f8f9fa;
        }

        .container {
            margin-top: 50px;
        }

        form {
            background-color: #fff;
            padding: 20px;
            border-radius: 5px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }

        label {
            font-weight: bold;
        }

        input[type="text"] {
            width: 100%;
            padding: 8px;
            margin-bottom: 10px;
            box-sizing: border-box;
        }

        input[type="submit"] {
            background-color: #007bff;
            color: #fff;
            padding: 10px 20px;
            border: none;
            border-radius: 5px;
            cursor: pointer;
        }
    </style>
    <!-- Include necessary stylesheets and scripts -->
</head>
<body>
    <?php
        $servername = "localhost";
        $username = "root";
        $password = "";
        $database = "ltweb";

        try {
            $conn = new PDO("mysql:host=$servername;dbname=$database", $username, $password);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $studentId = $_GET['id'];
            $sql = "SELECT * FROM student WHERE Id = :studentId";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':studentId', $studentId);
            $stmt->execute();
            $result = $stmt->fetch(PDO::FETCH_ASSOC);

        } catch(PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
        $conn = null;
    ?>
    <form method="post" action="save_update.php">
        <input type="hidden" name="id" value="<?php echo $result['Id']; ?>">
        <label>Tên sinh viên:</label>
        <input type="text" name="fullName" value="<?php echo $result['FullName']; ?>"><br>
        <!-- Thêm các input cho các thông tin cần sửa -->

        <input type="submit" name="submit" value="Xác nhận">
    </form>
</body>
</html>
