<!DOCTYPE html>
<html lang="en">
<head>
    <title>Danh sách sinh viên</title>
    <link rel="stylesheet" type="text/css" href="indexforstudentlist.css">
    <style>
        td, th {
            border: 1px solid #000000;
            text-align: center;
            padding: 8px;
        }
    </style>
</head>
<?php 
    $servername = "localhost";
    $username = "root";
    $password = "";
    $database = "ltweb";

    try {
        $conn = new PDO("mysql:host=$servername;dbname=$database", $username, $password);

        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $sql = "SELECT * FROM student" ;
    
        $stmt = $conn->prepare($sql);
        $stmt-> execute();
        $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $result = $stmt->fetchAll();

    } catch(PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
    $conn = null;

?>
<body>
<form id="form" class="list" method="post" enctype="multipart/form-data">
    <div class="element-search">
        <label class="text-label"> Khoa </label>
        <label>
            <select class="search" id="select-option">
                <option id="option"></option>
            </select>
        </label>
        <div class="position-key">
            <label class="text-label">
                Từ khóa
            </label>
            <label>
                <input type="text" id="search-text" class="search" placeholder="Nhập từ khóa">
            </label>
        </div>
        <button type="button" onclick="searchData()" class="button"> Tìm kiếm</button>
    </div>
    <div class="element-display">
        <span>
            Số sinh viên tìm thấy: <span id="count"></span>
        </span>
        <a href="register.php">
            <input type="button" id="button-add" class="button" value="Thêm">
        </a>
    </div>
    <div class="element-table">
        <table id="table" class="table">
            <thead>
            <tr>
                <th>No</th>
                <th>Tên sinh viên</th>
                <th>Khoa</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody id="resultBody">
            <?php
            foreach($result as $key => $value){

            ?>  <tr>
                <th><?php echo $value['Id']; ?></th>
                <th><?php echo $value['FullName']; ?></th>
                <th><?php echo $value['khoa']; ?></th>
                <th>
                    <a href="update_students.php?id=<?php echo $value['Id']; ?>" class="buttonedit">Edit</a>
                    <a href="#" onclick="confirmDelete(<?php echo $value['Id']; ?>)" class="buttondelete">Delete</a>
                </th>

            </tr>
                <?php }?>
            </tbody>
        </table>
    </div>

</form>

<script>
    function confirmDelete(studentId) {
        var r = confirm("Bạn muốn xóa sinh viên này?");
        if (r == true) {
            // Gửi yêu cầu xóa đến server sử dụng Ajax
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    // Xóa dòng trên trang web sau khi xóa thành công
                    var row = document.getElementById("row_" + studentId);
                    row.parentNode.removeChild(row);
                }
            };
            xhttp.open("GET", "delete_student.php?id=" + studentId, true);
            xhttp.send();
        }
    }
</script>
</body>
</html>
