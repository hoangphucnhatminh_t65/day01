<?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $database = "ltweb";

    $studentId = $_GET['id'];

    try {
        $conn = new PDO("mysql:host=$servername;dbname=$database", $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $sql = "DELETE FROM student WHERE Id = :studentId";
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':studentId', $studentId);
        $stmt->execute();
        
        echo "Xóa thành công";
    } catch(PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
    $conn = null;
?>
