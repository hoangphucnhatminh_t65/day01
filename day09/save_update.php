<?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $database = "ltweb";

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $studentId = $_POST['id'];
        $fullName = $_POST['fullName'];
        // Thêm các dòng code để lấy thông tin từ các input khác

        try {
            $conn = new PDO("mysql:host=$servername;dbname=$database", $username, $password);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $sql = "UPDATE student SET FullName = :fullName WHERE Id = :studentId";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':fullName', $fullName);
            $stmt->bindParam(':studentId', $studentId);
            $stmt->execute();

            // Thêm các dòng code để cập nhật các thông tin khác

            header("Location: student_list.php"); // Chuyển về trang danh sách sinh viên sau khi cập nhật thành công

        } catch(PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
        $conn = null;
    }
?>
