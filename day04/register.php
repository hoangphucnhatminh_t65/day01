<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<form class="form" id="form" onsubmit="return validateForm()">
    <div class="message" id="message">
        <p id="error-message" class="error-message"></p>
    </div>
    <div class="center-form">
        <div class="table">
            <label class="label"> Họ và Tên <span class="validate">*</span> </label>
            <label>
                <input type="text" id="ho_ten" class="input" required>
            </label>
        </div>
        <div class="table">
            <label class="label"> Giới tính <span class="validate">*</span> </label>
            <div class="radio">
                <?php
                $genderOptions = array(0 => "Nam", 1 => "Nữ");
                foreach ($genderOptions as $key => $value) {
                    echo '<input required type="radio" id="gioi_tinh' . $key . '" name="gioi_tinh" value="' . $key . '" ' . '>';
                    echo '<label for="gioi_tinh_' . $key . '">' . $value . '</label>';
                }
                ?>
            </div>
        </div>
        <div class="table">
            <label class="label"> Phân Khoa <span class="validate">*</span> </label>
            <label>
                <select name="state" class="Department" id="Department" required>
                    <option disabled selected hidden value=""> --Chọn phân khoa--</option>
                    <?php
                    $departments = array("MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
                    foreach ($departments as $key => $value) {
                        echo '<option value= "' . $key . '">' . $value . '</option>';
                    }
                    ?>
                </select>
            </label>
        </div>
        <div class="table">
            <label class="label"> Ngày sinh <span class="validate">*</span> </label>
            <label>
                <input type="date" class="Department" id="ngay_sinh" required>
            </label>
        </div>
        <div class="table">
            <label class="label"> Địa chỉ </label>
            <label>
                <input type="text" class="address">
            </label>
        </div>
        <div class="center-btn">
            <input type="submit" id="submit" class="submit-btn" value="Đăng Ký">
        </div>
    </div>
</form>
<script>
    const hoTenInput = document.getElementById('ho_ten');
    const gioiTinhInputs = document.querySelectorAll('input[name="gioi_tinh"]');
    const departmentSelect = document.getElementById('Department');
    const ngaySinhInput = document.getElementById('ngay_sinh');
    const errorMessageDiv = document.getElementById('error-message');
    const messageDiv = document.getElementById('message');
    const form = document.getElementById('form');

    document.getElementById("submit").addEventListener('click', function () {
        const fullName = hoTenInput.value;
        const gender = document.querySelector('input[name="gioi_tinh"]:checked');
        const department = departmentSelect.value;
        const dateOfBirth = ngaySinhInput.value;
        let count = 0;
        let errorMessage = '';
        let errorFlag = false;

        if (fullName === '') {
            errorMessage += 'Hãy nhập tên .<br>';
            errorFlag = true;
            count += 1;
        }

        if (!gender) {
            errorMessage += 'Hãy chọn giới tính .<br>';
            errorFlag = true;
            count += 1;
        }

        if (department === '') {
            errorMessage += 'Hãy chọn phân khoa .<br>';
            errorFlag = true;
            count += 1;
        }

        if (dateOfBirth === '') {
            errorMessage += 'Hãy nhập ngày sinh .<br>';
            errorFlag = true;
            count += 1;
        }

        if (errorFlag) {
            errorMessageDiv.innerHTML = errorMessage;
        }

        switch (count) {
            case 1:
                messageDiv.style.height = '30px';
                form.style.height = '450px';
                break;
            case 2:
                messageDiv.style.height = '60px';
                form.style.height = '500px';
                break;
            case 3:
                messageDiv.style.height = '90px';
                form.style.height = '530px';
                break;
            case 4:
                messageDiv.style.height = '120px';
                form.style.height = '550px';
                break;
            default:
                break;
        }
    })

    function validateForm() {
        const selectedDepartment = departmentSelect.value;
        return selectedDepartment !== '';
    }
</script>
</body>
</html>
